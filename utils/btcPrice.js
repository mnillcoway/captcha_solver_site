/**
 * Created by ilya on 04/03/2017.
 */
var redis = require('../redisSingleTon');
var HttpError = require('../error').HttpError;
var request = require('request');

var callbacks = [], inProgress = false;
function getBlockchainInfoPrice(callback) {
    callbacks.push(callback);
    if (!inProgress) {
        inProgress = true;
        request('https://blockchain.info/ticker', function (err, response, body) {
            if (!err && body) {
                try {
                    body = JSON.parse(body);
                    if (body && 'USD' in body && 'last' in body.USD && !isNaN(parseFloat(body.USD.last))) {
                        return redis.multi().set('btcPrice', body.USD.last).expire('btcPrice', 60).set('lastBtcPrice', body.USD.last).exec(function (err) {
                            inProgress = false;
                            callbacks.forEach(function (callback) {
                                setTimeout(function () {
                                    callback(null, body.USD.last)
                                })
                            });
                            callbacks = [];
                        });
                    }
                } catch (e) {
                    console.log('error to parse blockchain.info answer', e);
                }
            } else {
                inProgress = false;
                callbacks.forEach(function (callback) {
                    setTimeout(function () {
                        callback(err || new Error('bad answer from blockchain.info'))
                    })
                });
                callbacks = [];
            }
        })
    }
}

module.exports.getPrice = function (callback) {
    redis.get('btcPrice', function (err, btc) {
        if (err) {
            callback(err);
        } else {
            if (btc && !isNaN(parseFloat(btc))) {
                callback(null, parseFloat(btc));
            } else {
                getBlockchainInfoPrice(function (err, price) {
                    if (err || isNaN(parseFloat(price))) {
                        redis.get('lastBtcPrice', function (err, price) {
                            if (err || isNaN(parseFloat(price))) {
                                callback(err || new Error('No last price for btc'));
                            } else {
                                callback(null, price);
                            }
                        });
                    } else {
                        callback(null, price);
                    }
                });
            }
        }
    });
};