/**
 * Created by ilya on 22/02/2017.
 */

var redis = require('../redisSingleTon');
var HttpError = require('../error').HttpError;

module.exports.limit = function (name, counter, timeout) {
    return function (req, res, next) {
        redis.multi().incr('limiter:' + name + ':' + req.ip).expire('limiter:' + name + ':' + req.ip, timeout).exec(function (err, count) {
            count = count | 0;
            if (!err && count > counter) {
                redis.setex('banned:' + req.ip, timeout, 1, function (err) {
                    next(err || new HttpError(429, 'Too Many Requests'));
                });
            } else {
                next(err);
            }
        })
    }
};