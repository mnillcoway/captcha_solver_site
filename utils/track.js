const request = require('request');

function track(id, event) {
    if (!event || !id) {
        console.log('Wrong tracking call');
        return;
    }
    request('http://track.likesmafia.com/event/57c87e2635ef237c02f28368/' + id + '/' + event + '/0/0?no_chache=' + Date.now() + Math.random(), function () {});
}
module.exports = track;