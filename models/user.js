"use strict";

var async = require('async');

var redis = require('../redisSingleTon');
var HttpError = require('../error').HttpError;
var config = require('../config');
var moment = require('moment');
var changeApiKeyProcedure = require('../redisSingleTon/procedures/changeApiKey');
var sendRegistrationMessage = require('../email').sendRegistrationMessage;

function User(id) {
    this.id = id;
    this.balance = 0;
    this.solvedAllTime = 0;
    this.email = undefined;
    this.apiKey = undefined;
    this.paymentsAddress = undefined;
    this.paymentsHistory = [];
}

User.byId = function (id) {
    return new User(id);
};

User.prototype.loadProfile = function (callback) {
    async.parallel([
        this.loadBalance.bind(this),
        this.loadSolvedAllCount.bind(this),
        this.loadEmail.bind(this)
    ], callback);
};

User.prototype.loadBalance = function (callback) {
    redis.hget('balance', this.id, function (err, balance) {
        this.balance = balance | 0;
        callback(err);
    }.bind(this))
};

User.prototype.loadSolvedAllCount = function (callback) {
    redis.hget('solved', this.id, function (err, count) {
        this.solvedAllTime = count | 0;
        callback(err);
    }.bind(this))
};

User.prototype.loadEmail = function (callback) {
    redis.hget('email_by_id', this.id, function (err, email) {
        this.email = email;
        callback(err || (!email ? new HttpError(500, 'Internal error') : null));
    }.bind(this))
};

User.prototype.loadApiKey = function (callback) {
    redis.hget('api_key', this.id, function (err, apiKey) {
        this.apiKey = apiKey;
        callback(err);
    }.bind(this))
};

User.prototype.regenerateApiKey = function (callback) {
    var apiKey = '';
    for (var i = 0; i < 6; i ++)
        apiKey += Math.random().toString(36).slice(-8);
    changeApiKeyProcedure.eval('' + this.id, apiKey, function (err, apiKey) {
        if (err) {
            callback(err);
        } else {
            this.apiKey = apiKey;
            callback(null);
        }
    }.bind(this))
};

User.prototype.loadPayments = function (callback) {
    async.parallel([
        function () {
            redis.smembers('userPayments:'+this.id, function (err, listOfPayments) {
                if (err || listOfPayments.length  === 0) {
                    callback(err);
                } else {
                    var pipe = redis.multi();
                    listOfPayments.forEach(function (paymentId) {
                        pipe = pipe.hget('userPayment', paymentId);
                    });
                    pipe.exec(function (err, data) {
                        if (err) {
                            callback(err);
                        } else {
                            data.forEach(function (answer) {
                                try {
                                    answer = JSON.parse(answer);
                                    answer.date = moment(answer.date).format('MM-DD-YYYY');
                                    this.paymentsHistory.push(answer);
                                } catch (e) {
                                    console.log('fail to parse payment', e, answer);
                                }
                            }.bind(this));
                            callback(null);
                        }
                    }.bind(this))
                }
            }.bind(this))
        }.bind(this)
    ],callback);
};

User.prototype.loadDailyHistory = function (callback) {
    var multi = redis.multi();
    var to = new Date();
    to = new Date(+to - to.getMilliseconds() - to.getSeconds()*1000 - to.getMinutes()*60*1000  - to.getHours()*60*60*1000 + 1000*60*60*24);
    for (var i = 0; i < 31; i++) {
        var date = +to - i * 1000*60*60*24;
        multi = multi.hget('userUsage:' + date, this.id)
    }
    multi.exec(function (err, data) {
        if (err) {
            callback(err);
        } else {
            var returnData = [];
            for (var i = 0; i < 31; i++) {
                var date = +to - i * 1000 * 60 * 60 * 24 - 1000*60*60*24;
                returnData.push({date:moment(date).format('MM-DD-YYYY'), count:data[i] || 0});
            }
            callback(null, returnData);
        }
    })
};

User.prototype.loadAll = function (callback) {
    async.parallel([
        this.loadBalance.bind(this),
        this.loadEmail.bind(this),
        this.loadSolvedAllCount.bind(this),
        // this.loadRefs.bind(this),
        // this.loadUsername.bind(this),
        // this.loadToken.bind(this)
    ], callback);
};

//тут будет создание сообщений для пользователя, который он увидит в аппе типо new message.
User.prototype.createMessage = function (message) {

};

// User.prototype.addCoins = function (coins, callback) {
//     redis.hincrby("balance", this.id, coins, function (err, count) {
//         this.balance = count;
//         callback(err, count);
//     }.bind(this))
// };

//возвращает err, User, isNew
User.create = function (email, balance, callback) {
    email = email.toLowerCase();
    redis.incr('user_id_counter', function (err, id) {
        var password = Math.random().toString(36).slice(-8);
        var apiKey = '';
        for (var i = 0; i < 6; i ++)
            apiKey += Math.random().toString(36).slice(-8);
        if (err) {
            callback(err);
        } else {
            redis.multi().hset('id_by_email', email, id).sadd('users', id).hset('email_by_id', id, email).hset('balance', id, balance).incrby('balance:all', balance).hset('password', id, password).hset('api_key', id, apiKey).hset('id_by_api_key', apiKey, id).bgsave().exec(function(err, data) {
                if (err) {
                    callback(err);
                } else {
                    var user = new User(id);
                    user.password = password;
                    user.loadProfile(function (err) {
                        if (err) {
                            callback(err);
                        } else {
                            sendRegistrationMessage(user, callback);
                        }
                    });
                }
            });
        }
    });
};
module.exports = User;