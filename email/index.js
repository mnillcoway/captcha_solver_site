var config = require('../config');
// var curl = require('curlrequest');
var Mailgun = require('mailgun-js');
var mailgun = new Mailgun({apiKey: "key-d96c7b77147b3a5e70a4ab5e679459a2", domain: "mail.mechanicalmill.com"});

function sendRegistrationMessage (user, callback){
    sendEmail("no-reply@mail.mechanicalmill.com", user.email, "Your credentials",
        `<b>Welcome stranger,<br/></b><br /> Your email account is : ${user.email} <br /> Your password is: ${user.password}<br/><br/> Best wishes, <br/>MechanicalMill.com`, callback);
}
exports.sendRegistrationMessage  = sendRegistrationMessage;

var sendEmail = function(from, to, subject, html, callback) {
    var data =  {
        "from": from,
        "to": to,
        "subject": subject,
        "html" : html
    };
    mailgun.messages().send(data, callback);
};
exports.sendEmail  = sendEmail;