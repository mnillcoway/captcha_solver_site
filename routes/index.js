"use strict";

var express = require('express');
var router = express.Router();
var User = require('../models/user');
var HttpError = require('../error').HttpError;
var ValidationError = require('../error').ValidationError;
var limiter = require('../utils/requestLimiter').limit;
var config = require('../config');
var async = require('async');
var bodyParser = require('body-parser');
var redis = require('../redisSingleTon');
var track = require('../utils/track');

router.get('/blockchaininfo', require('./blockchaininfo'));
router.use(bodyParser.urlencoded({extended: true}));

//проверяем подписаную куку и загржаем юзера
router.use(function (req, res, next) {
    if (req.session && 'userid' in req.session && (typeof req.session.userid === 'string' || req.session.userid instanceof String) && req.session.userid.length > 0) {
        req.user = User.byId(req.session.userid);
        req.user.loadProfile(function (err) {
            if (err) {
                next(err);
            } else {
                if (req.user.email === config.get('admin_email'))
                    req.user.isAdmin = true;
                next();
            }
        });
    } else {
        req.user = null;
        next();
    }
});

var limiterForLogin = limiter('login', 10, 60);
router.post('/login', function (req, res, next) {
    async.waterfall([
        function (callback) {
            req.checkBody('email', 'Bad email or password').notEmpty().isString();
            req.checkBody('password', 'Bad email or password').notEmpty().isString();
            var err = req.validationErrors();
            callback(err ? new ValidationError(err) : null);
        },
        function (callback) {
            redis.hget('id_by_email', req.body.email.toLowerCase(), callback);
        },
        function (id, callback) {
            if (id) {
                redis.hget('password', id, function (err, password) {
                  if (err || password !== req.body.password) {
                    callback(err || new HttpError(400, 'Bad email or password'));
                  } else {
                      req.session.userid = id;
                      callback();
                  }
                });
            } else {
                callback(new HttpError(400, 'Bad email or password'));
            }
        }
    ], function(err, result) {
        if (err) {
            limiterForLogin(req, res, function (err3) {
                res.status(401).render('login.ejs', {err: err.message || 'Internal server error', email: req.body.email || ''});
            });
        } else {
            res.redirect(302, config.get('url'));
        }
    });
});

router.get('/login', limiter('user-get', 100, 60), function (req, res, next) {
    res.render('login.ejs', {email: ''});
});

router.post('/register', limiter('register', 10, 6000), function (req, res, next) {
    return next();
    async.waterfall([
        function (callback) {
            req.checkBody('email', 'Bad email or password').notEmpty().isEmail();
            var err = req.validationErrors();
            callback(err ? new ValidationError(err) : null);
        },
        function (callback) {
            redis.hget('id_by_email', req.body.email.toLowerCase(), callback);
        },
        function (id, callback) {
            if (id) {
                callback(new HttpError(400, 'Email already taken'));
            } else {
                User.create(req.body.email, 0, callback);
            }
        }
    ], function(err, result) {
        if (err) {
            res.status(400).render('register.ejs', {err: err.message || 'Internal server error', email: req.body.email || '', message:''});
        } else {
            res.render('register.ejs', {email: req.body.email, err:'', message:'We just drop u email.'});
        }
    });
});

router.get('/register', limiter('user-get', 100, 60), function (req, res, next) {
    res.render('register.ejs', {email: '', err:'', message:''});
});

router.use('/admin', limiter('admin', 100, 60), mustBeAdmin, require('./admin'));
router.use('/user',  limiter('user-actions', 1000, 60), mustBeLoggedIn, require('./user'));
router.get('/', limiter('user-actions', 100, 60), function (req, res, next) {
   if ('user' in req) {
       res.redirect(302, config.get('url') + '/user/dashboard');
   } else {
       res.redirect(302, config.get('url') + '/login');
   }
});
module.exports = router;

function mustBeLoggedIn(req, res, next) {
    // req.user = User.byId(1);
    // req.user.loadProfile(next);
    // return;
    next(req.user ? null : new HttpError(401));
}

function mustBeAdmin(req, res, next) {
    if ('admin_key' in req.body && req.body.admin_key === config.get('admin_key'))
        next();
    else
        next(new HttpError(404));
}