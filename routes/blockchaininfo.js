"use strict";

var express = require('express');
var router = express.Router();
var User = require('../models/user');
var request = require('request');
var paymentProcedure = require('../redisSingleTon/procedures/payment');

var HttpError = require('../error').HttpError;
var ValidationError = require('../error').ValidationError;
var async = require('async');
var config = require('../config');
var redis = require('../redisSingleTon');
var btcPrice = require('../utils/btcPrice');

function blockchaininfo(req, res, next) {
    var address = req.query.address;
    var secret = req.query.secret;
    var confirmations = parseInt(req.query.confirmations);
    var tx_hash = req.query.transaction_hash;
    var value = parseFloat(req.query.value)/100000000;
    var userid = req.query.userid;

    if (address && !isNaN(confirmations) && !isNaN(value) && userid) {
        redis.hget('bitcoinhash', userid, function (err, secretStored) {
            if (err) {
                next(err);
            } else if (secret !== secretStored) {
                res.end('*ok*');
            } else if (confirmations < 3) {
                res.end('*bad*');
            } else {
                btcPrice.getPrice(function (err, price) {
                    if (err || !price) {
                        next(err || new HttpError(500));
                    } else {
                        paymentProcedure.eval(userid, '' + parseInt(value*parseFloat(price) * 5000), JSON.stringify({userid:userid, btc:value, captches:parseInt(value*parseFloat(price)*5000), date: Date.now(), confirmations:confirmations, usd:(value*parseFloat(price)).toFixed(3), hash:tx_hash, address:address}), function (err) {
                            if (err)
                                next(err);
                            else
                                res.end('*ok*');
                        })
                    }
                })
            }
        });
    } else {
        res.end('*bad*');
    }
}
module.exports = blockchaininfo;