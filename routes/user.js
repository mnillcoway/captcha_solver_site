"use strict";

var express = require('express');
var router = express.Router();
var User = require('../models/user');
var request = require('request');

var HttpError = require('../error').HttpError;
var ValidationError = require('../error').ValidationError;
var async = require('async');
var config = require('../config');
var redis = require('../redisSingleTon');
var btcPrice = require('../utils/btcPrice');

router.get('/dashboard', function (req, res, next) {
    req.user.loadDailyHistory(function (err, history) {
        if (err) {
            next(err);
        } else {
            res.render('dashboard.ejs', {
                page: 'dashboard',
                history: history,
                balance: req.user.balance,
                solvedAllTime: req.user.solvedAllTime
            });
        }
    });
});

router.get('/settings', function (req, res, next) {
    req.user.loadApiKey(function (err) {
        if (err) {
            next(err);
        } else {
            res.render('settings.ejs', {
                page: 'settings',
                apiKey: req.user.apiKey || 'null'
            });
        }
    })
});

router.post('/regenerateApiKey', function (req, res, next) {
    req.user.regenerateApiKey(function (err) {
        if (err) {
            next(err);
        } else {
            res.redirect(302, config.get('url') + '/user/settings');
        }
    })
});

router.get('/payments', function (req, res, next) {
    btcPrice.getPrice(function (err, price) {
        if (err)
            next(err);
        else {
            req.user.loadPayments(function (err, history) {
                if (err) {
                    next(err);
                } else {
                    res.render('payments.ejs', {
                        page: 'payments',
                        price: price,
                        address: req.user.paymentsAddress,
                        history: req.user.paymentsHistory
                    });
                }
            });
        }
    });
});

router.get('/payment', function (req, res, next) {
    redis.hget('bitcoin:currentaddress', req.user.id, function (err, address) {
        if (err)
            next(err);
        else if (address) {
            res.render('payment.ejs', {
                page:'payment',
                address: address,
                message: 'We still wait u payment or confirmations for:'
            });
        } else {
            redis.setex('bitcoinmutex:'+req.user.id, 30, 1, function (err, seted) {
                if (err || !seted) {
                    next(err || new HttpError(429, 'Too Many Requests'))
                } else {
                    var secretKey = '' + (1000000000 * Math.random());
                    redis.hset('bitcoinhash', req.user.id, secretKey, function (err) {
                        if (err) {
                            next(err);
                        } else {
                            request('https://api.blockchain.info/v2/receive?xpub=' + config.get('blockchain:xpub') + '&callback='+ encodeURIComponent('https://mechanicalmill.com/blockchaininfo?userid='+req.user.id + '&secret=' + secretKey) +'&key='+ config.get('blockchain:apikey'), function (err, response, body) {
                                if (err || !response || response.statusCode != 200 || !body) {
                                    next(err || new HttpError(500, 'Internal server error'));
                                } else {
                                    try {
                                        body = JSON.parse(body);
                                    } catch (e) {
                                        return next(new HttpError(500, 'Internal server error'));
                                    }
                                    if (body && body.address) {
                                        redis.hset('bitcoin:currentaddress', req.user.id, body.address, function (err) {
                                            if (err) {
                                                next(err);
                                            } else {
                                                res.render('payment.ejs', {
                                                    page:'payment',
                                                    address: body.address,
                                                    message: 'Just sent bitcoins to:'
                                                });
                                            }
                                        })
                                    } else {
                                        next(new HttpError(500, 'Internal server error'));
                                    }
                                }
                            });
                        }
                    })
                }
            })
        }
    });
});
module.exports = router;