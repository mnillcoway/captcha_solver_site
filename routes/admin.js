"use strict";

var express = require('express');
var router = express.Router();
var User = require('../models/user');

var HttpError = require('../error').HttpError;
var ValidationError = require('../error').ValidationError;
var async = require('async');
var config = require('../config');
var redis = require('../redisSingleTon');

router.post('/createUser', function (req, res, next) {
    async.waterfall([
        function (callback) {
            req.checkBody('email', 'Email is empty').notEmpty().isString();
            req.checkBody('balance', 'Balance is empty').notEmpty().isNumber();
            var err = req.validationErrors();
            callback(err ? new ValidationError(err) : null);
        },
        function (callback) {
            redis.hget('id_by_email', req.body.email.toLowerCase(), callback);
        },
        function (id, callback) {
            if (!id) {
                User.create(req.body.email, req.body.balance, callback);
            } else {
                callback(new HttpError(400, 'User with this email is already registered'));
            }
        }
    ], function(err, result) {
        if (err) {
            next(err);
        } else {
            res.json(result);
        }
    });
});
module.exports = router;

