var path = require('path');
var util = require('util');
var http = require('http');

// ошибки для выдачи посетителю
function HttpError(status, message) {
    if (this.constructor !== HttpError)
        return new HttpError(status, message);
    Error.apply(this, arguments);
    Error.captureStackTrace(this, HttpError);

    this.status = status;
    this.message =  message || http.STATUS_CODES[status] || "Error";
}

util.inherits(HttpError, Error);
HttpError.prototype.name = 'HttpError';
exports.HttpError = HttpError;


function ValidationError(error) { //express-validator error
    if (this.constructor !== ValidationError)
        return new ValidationError(error);

    var message = '';
    Error.apply(this, arguments);
    Error.captureStackTrace(this, ValidationError);
    this.code = 400;
    this.status = 400;
    message += error[0] && error[0].msg;
    this.message = message || 'Validation error';
}
util.inherits(ValidationError, Error);
ValidationError.prototype.name = 'ValidationError';
exports.ValidationError = ValidationError;