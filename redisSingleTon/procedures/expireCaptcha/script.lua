local time = ARGV[1];
local data = redis.call('zrangebyscore', 'expire:tasks', 0,  time);

for i = 1, #data do
    local userId = string.sub(data[i], 0, string.find(data[i], '_', 1, true) - 1);
    local taskId = string.sub(data[i], string.find(data[i], '_', 1, true) + 1);
    redis.call('zrem', 'user:tasks_list:' .. userId, taskId);
    redis.call('zrem', 'expire:tasks', data[i]);
    redis.call('hdel', 'user:tasks:' .. userId, taskId);
end;