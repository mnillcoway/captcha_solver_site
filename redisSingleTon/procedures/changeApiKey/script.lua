local userId = ARGV[1];
local apiKey = ARGV[2];

local check_busy = tonumber(redis.call('hget', 'id_by_api_key', apiKey));
if check_busy ~= nil then
    return redis.call('hget', 'api_key', userId);
else
    local old_key = redis.call('hget', 'api_key', userId);
    redis.call('hdel', 'id_by_api_key', old_key);
    redis.call('hset', 'api_key', userId, apiKey);
    redis.call('hset', 'id_by_api_key', apiKey, userId);
    return apiKey;
end;