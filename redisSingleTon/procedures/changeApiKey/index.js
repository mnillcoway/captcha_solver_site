var redis = require('../../index.js');
var fs = require('fs');
var path = require('path');

var sha = null;

function init(callback) {
    var script = fs.readFileSync(path.resolve(__dirname, 'script.lua'), 'utf8');
    redis.script('load', script, function (err, hash) {
        sha = hash;
        callback(err);
    })
}
module.exports.init = init;

function eval() {
    redis.evalsha.apply(redis, [sha, 0].concat([].slice.call(arguments)));
}
module.exports.eval = eval;