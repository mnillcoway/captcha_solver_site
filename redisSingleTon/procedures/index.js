"use strict";

var async = require('async');
var path = require('path');

var procedures = ['changeApiKey', 'payment', 'expireCaptcha'];
var alreadyInited = false;
var config = require('../../config');
var expire = require('./expireCaptcha');

function init(callback) {
    if (alreadyInited)
        throw new Error('Must be called once!');

    alreadyInited = true;

    async.eachSeries(procedures, function (procedureName, callback) {
        var procedure = require(path.resolve(__dirname, procedureName));
        procedure.init(callback);
    }, function (err) {
        if (!err)
            setInterval(function () {
                expire.eval(Date.now()-1000*60*60*2);
            }, 30000);
        callback(err);
    })
}
module.exports.init = init;