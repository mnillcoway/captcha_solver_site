local userId = ARGV[1];
local coins = ARGV[2];
local history = ARGV[3];

local address = redis.call('hget', 'bitcoin:currentaddress', userId)
if address ~= nil then
    redis.call('hdel', 'bitcoin:currentaddress', userId)
    redis.call('hdel', 'bitcoinhash', userId)
    redis.call('hincrby', 'balance', userId, coins);

    redis.call('sadd', 'userPayments:' .. userId, address)
    redis.call('hset', 'userPayment', address, history)
    return address;
else
    return 0;
end;