var request = require('request');
var config = require('./config');
var qs = require('querystring');

var email = process.argv[2];
var balance = parseInt(process.argv[3]) || 300;
var admin_key = '123n56n2838ancq8we812UASDIUasidunu2ebu1y2beasd';

if (!validateEmail(email))
    throw new Error('wrong email');

request.post('https://mechanicalmill.com/admin/createUser', {form: {email:email, admin_key:admin_key, balance:balance}}, function (err, status, data) {
    console.log(err, data);
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
