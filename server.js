var express = require('express');
var session = require('express-session');
var config = require('./config');
var logger = require('morgan');
var expressValidator = require('express-validator');
var Address6 = require('ip-address').Address6;

var routes = require('./routes/index');
var app = express();
var path = require('path');
var ejs = require('ejs');
var redis = require('./redisSingleTon');

var moment = require('moment');
var util = require('util');

var HttpError = require('./error').HttpError,
    ValidationError = require('./error').ValidationError;

app.set('port', config.get("port"));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', ejs);

app.locals.fromDate = function(date){
    return moment(date).format('HH:mm DD-MM-YY');
};

if (config.get('env') === 'production') {
   app.use(logger('combined', {
       skip: function (req, res) {
           return res.statusCode < 400
       }
   }));
} else {
    app.use(logger('dev'));
}

app.use(expressValidator({
    customValidators: {
        gte: function(param, num) {
            return param >= num;
        },
        isString: function(value){
            return typeof value === 'string' || value instanceof String;
        },
        isNumber: function(value){
            return !isNaN(parseFloat(value));
        }
    }
}));

app.get('/recaptcha.html', function (req, res, next) {
    res.render('recaptcha.ejs', {sitekey:req.query.sitekey});
});

app.use(session({
    saveUninitialized: true,
    resave: true,
    secret: config.get('session:secret'),
    key: config.get('session:key'),
    cookie: config.get('session:cookie')
}));

app.use(function (req, res, next) {
    var ip = (req.headers['x-forwarded-for'] || req.headers['x-ip'] || req.connection.remoteAddress).split(',')[0];
    if (ip && ip.indexOf(':') !== -1) {
        var add = new Address6(ip);
        if (add && add.valid) {
            ip = add.parsedAddress.slice(0, 4).join(':')
        }
    }
    req.ip = ip;
    redis.get('banned:' + req.ip, function (err, count) {
        count = count | 0;
        next(err || (count > 0 ? new HttpError(429, 'Too Many Requests') : null));
    })
});
app.use(routes);
app.use(express.static(__dirname + '/public'));
app.use(function(req, res, next) {
    var err = new HttpError(404, 'Not Found');
    next(err);
});

app.use(function(err, req, res, next) {
    console.log(err);
    var message;
    if (err instanceof ValidationError) {
        message = err.message;
        // } else if ((err instanceof MongooseError)||(err.name == 'MongoError')) {
    //     message.message = 'Internal server error';
    } else if (err instanceof HttpError) {
        message = err.message;
    } else {
        message = 'Internal server error';
    }
    if (req.accepts('html')) {
        if (err.status === 401) {
            res.redirect(302, '/login');
        } else {
            res.status(err.status || 500).render('error.ejs', {message:message, status:err.status})
        }
    } else {
        res.status(err.status || 500);
        res.json({message:message});
    }
});

require('./redisSingleTon/procedures').init(function (err) {
    if (err)
        throw err;
    var server = app.listen(config.get('port'), function() {
        console.log('Express server listening on port ' + server.address().port);
    });
});

if (config.get('env') === 'production') {
    console.log("server has been restarted", new Date());
}